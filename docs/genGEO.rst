genGEO package
==============

Subpackages
-----------

.. toctree::
   :maxdepth: 4

   genGEO.src
   genGEO.tests
   genGEO.utils

Submodules
----------

genGEO.exampleCPGSystemStudy module
-----------------------------------

.. automodule:: genGEO.exampleCPGSystemStudy
   :members:
   :undoc-members:
   :show-inheritance:

genGEO.testSuite module
-----------------------

.. automodule:: genGEO.testSuite
   :members:
   :undoc-members:
   :show-inheritance:

Module contents
---------------

.. automodule:: genGEO
   :members:
   :undoc-members:
   :show-inheritance:
